package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/hello")
	String hello() {
		return "Hello world!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public String testMethod(int a, int b, int c, int d, int e, int f, int j, int k) {
		System.out.println("This is a test method");
		return "This is a test method";
	}

	public int add(int a, int b, int c) {
		int sum = a + b;
		return sum;
	}
}
